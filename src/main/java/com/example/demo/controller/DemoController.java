package com.example.demo.controller;

import com.example.demo.annotation.Docket1;
import com.example.demo.entity.Book;
import com.example.demo.util.I18nUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@Docket1
@Api(tags = "Demo API")
public class DemoController {

    private final I18nUtil i18nUtil;

    public DemoController(I18nUtil i18nUtil) {
        this.i18nUtil = i18nUtil;
    }

    @ApiOperation("查詢")
    @GetMapping(value = "/book")
    public Book book() {
        Book book = new Book();
        String msg1 = i18nUtil.getMessage("api.code.0001");
        book.setMsg1(msg1);

        String msg2 = i18nUtil.getMessage("api.code.0002", 99);
        book.setMsg2(msg2);

        return book;
    }

}
