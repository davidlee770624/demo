package com.example.demo.util;

import lombok.var;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.lang.Nullable;

import java.util.Locale;

public class I18nUtil {

    private final MessageSource messageSource;

    public I18nUtil(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getMessage(String code, @Nullable Object... args) {
        var locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(code, args, locale);
    }

    public String getMessage(String code) {
        var locale = LocaleContextHolder.getLocale();
        return messageSource.getMessage(code, null, locale);
    }
}
