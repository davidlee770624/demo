package com.example.demo.resolve;

import com.example.demo.constant.HeaderName;
import com.google.common.collect.Lists;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Locale;

public class I18nLocaleResolver implements LocaleResolver {

    private final List<Locale> AVAILABLE_LOCALES = Lists.newArrayList(Locale.JAPAN, Locale.US, Locale.TAIWAN);

    /**
     * 獲取請求中的語系
     **/
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String header = request.getHeader(HeaderName.LANGUANGE);
        if (StringUtils.hasText(header)) {
            String[] headerValues = StringUtils.tokenizeToStringArray(header, "_");
            if (headerValues.length == 2) {
                Locale locale = new Locale(headerValues[0], headerValues[1]);
                if (AVAILABLE_LOCALES.contains(locale)) {
                    return locale;
                }
            }
        }
        return Locale.US;
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
        int i = 0;
    }
}
