package com.example.demo.constant;

public interface HeaderName {

    String USER_NO = "userNo";
    String LANGUANGE = "language";
    String PLATFORM = "platform";
}
