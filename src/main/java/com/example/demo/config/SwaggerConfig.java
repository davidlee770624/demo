package com.example.demo.config;

import com.example.demo.annotation.Docket1;
import com.example.demo.constant.HeaderName;
import com.google.common.collect.Lists;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.RequestParameterBuilder;
import springfox.documentation.service.ParameterType;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    /**
     * Header
     **/
    private List headers = Lists.newArrayList(
            new RequestParameterBuilder()
                    .name(HeaderName.USER_NO)
                    .description("會員編號")
                    .in(ParameterType.HEADER)
                    .query(parameter -> parameter.defaultValue("124544"))
                    .build(),
            new RequestParameterBuilder()
                    .name(HeaderName.LANGUANGE)
                    .description("語系 [zh_TW|en_US]")
                    .query(parameter -> parameter.defaultValue("en_US"))
                    .in(ParameterType.HEADER)
                    .build(),
            new RequestParameterBuilder()
                    .name(HeaderName.PLATFORM)
                    .description("前端平台類型 [WEB|IOS|ANDROID|SERVER]")
                    .query(parameter -> parameter.defaultValue("WEB"))
                    .in(ParameterType.HEADER)
                    .build());

    /**
     * 頁籤1
     **/
    @Bean
    public Docket docket1() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(new ApiInfoBuilder().title("title docket 1").description("description docket 1").build())
                .groupName("group docket 1")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.demo.controller").and(RequestHandlerSelectors.withClassAnnotation(Docket1.class)))
                .paths(PathSelectors.any())
                .build()
                .globalRequestParameters(headers);
    }

}
