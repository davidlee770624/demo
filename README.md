# 個人紀錄
#### spring boot
- spring and spring.factories
- yml and profile
- application.yml only for local dev
#### swagger
- http://localhost:8080/swagger-ui/
#### spring mvc
- restful api
- header and i18n
- controller aspect
- response body advice
- error handler and http 2xx 4xx 5xx
- filter and mdc log
- json log
#### redis
- reddisson
- spel and cache annotation
- spel and redis lock
- redis service
#### rabbitmq
- send
- listen
#### jpa
- enum
- json
#### cicd
- gitlab-cy.yml
- docker file
- git tag and build docker image
- env's yml and deploy docker image



### Java Spring template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/spring).

### CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.